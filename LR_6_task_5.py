import numpy as np
import neurolab as nl

# Matrices for the initials I, M, and P
initials = {
     'And': [
         [1, 0, 1],
         [0, 1, 0],
         [0, 1, 0],
         [0, 1, 0],
         [1, 0, 1]
     ],
     'M': [
         [1, 1, 1],
         [1, 0, 1],
         [1, 0, 1],
         [1, 0, 1],
         [1, 0, 1]
     ],
     'P': [
         [1, 1, 1],
         [1, 0, 1],
         [1, 1, 1],
         [1, 0, 0],
         [1, 0, 0]
     ]
}

# Creating an array for training
train_data = []
for letter, matrix in initials.items():
     flattened = [pixel for row in matrix for pixel in row]
     train_data.append(flattened)

# Convert to numpy array and replace 0 with -1 (as in the previous task)
train_data = np.array(train_data)
train_data[train_data == 0] = -1

# Creation and training of the Hopfield network
net = nl.net.newhop(train_data)

# Validation of trained network
for letter, matrix in initials.items():
     flattened = [pixel for row in matrix for pixel in row]
     test_data = np.array(flattened)
     test_data[test_data == 0] = -1
     out = net.sim([test_data])
     print(f'Test on {letter}: {(out[0] == train_data[list(initials.keys()).index(letter)]).all()}')
