import numpy as np
import neurolab as nl

# Define the target patterns for training
target = [[-1, 1, -1, -1, 1, -1, -1, 1, -1],
          [1, 1, 1, 1, -1, 1, 1, -1, 1],
          [1, -1, 1, 1, 1, 1, 1, -1, 1],
          [1, 1, 1, 1, -1, -1, 1, -1, -1],
          [-1, -1, -1, -1, 1, -1, -1, -1, -1]]

# Define the input patterns for testing
input_data = [[-1, -1, 1, 1, 1, 1, 1, -1, 1],
              [-1, -1, 1, -1, 1, -1, -1, -1, -1],
              [-1, -1, -1, -1, 1, -1, -1, 1, -1]]

# Create and train the Hemming neural network
net = nl.net.newhem(target)

# Test the network on training data patterns
output = net.sim(target)
print("Test on train samples (must be [0, 1, 2, 3, 4])")
print(np.argmax(output, axis=0))

# Test the network on individual input patterns
output = net.sim([input_data[0]])
print("Outputs on recurrent cycle:")
print(np.array(net.layers[1].outs))

# Test the network on the input patterns
output = net.sim(input_data)
print("Outputs on test sample:")
