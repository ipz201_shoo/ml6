import numpy as np
import neurolab as nl

# Specify the matrices for the letters N, E, R, O
target = [[1, 0, 0, 0, 1,
            1, 1, 0, 0, 1,
            1, 0, 1, 0, 1,
            1, 0, 0, 1, 1,
            1, 0, 0, 0, 1],
           [1, 1, 1, 1, 1,
            1, 0, 0, 0, 0,
            1, 1, 1, 1, 1,
            1, 0, 0, 0, 0,
            1, 1, 1, 1, 1],
           [1, 1, 1, 1, 0,
            1, 0, 0, 0, 1,
            1, 1, 1, 1, 0,
            1, 0, 0, 1, 0,
            1, 0, 0, 0, 1],
           [0, 1, 1, 1, 0,
            1, 0, 0, 0, 1,
            1, 0, 0, 0, 1,
            1, 0, 0, 0, 1,
            0, 1, 1, 1, 0]]

chars = ['N', 'E', 'R', 'O']

# Convert matrix to -1 and 1 format
target = np.asfarray(target)
target[target == 0] = -1

# Creation and training of the Hopfield network
net = nl.net.newhop(target)

# Testing on training samples
output = net.sim(target)
print("Test on train samples:")
for i in range(len(target)):
     print(chars[i], (output[i] == target[i]).all())

# Testing on displaying the letter N with errors
print("\nTest on defaced N:")
test = np.asfarray([0, 0, 0, 0, 0,
                     1, 1, 0, 0, 1,
                     1, 1, 0, 0, 1,
                     1, 0, 1, 1, 1,
                     0, 0, 0, 1, 1])
test[test == 0] = -1
out = net.sim([test])
print((out[0] == target[0]).all(), 'Sim. steps', len(net.layers[0].outs))

print("\nTest on defaced N:")
test = np.asfarray([0, 0, 0, 0, 0,
                     1, 1, 0, 0, 1,
                     1, 1, 0, 0, 1,
                     1, 0, 1, 1, 1,
                     0, 0, 0, 1, 1])
test[test == 0] = -1
out = net.sim([test])
print((out[0] == target[0]).all(), 'Sim. steps', len(net.layers[0].outs))

print("\nTest on defaced E:")
test = np.asfarray([1, 0, 0, 0, 1,
                    1, 0, 0, 0, 1,
                    1, 1, 1, 1, 1,
                    1, 0, 0, 0, 0,
                    1, 0, 0, 0, 0])
test[test == 0] = -1
out = net.sim([test])
print((out[0] == target[0]).all(), 'Sim. steps', len(net.layers[0].outs))

print("\nTest on defaced R:")
test = np.asfarray([1, 1, 1, 1, 0,
                    1, 0, 0, 0, 1,
                    1, 1, 1, 1, 0,
                    1, 0, 0, 0, 1,
                    1, 0, 0, 1, 1])
test[test == 0] = -1
out = net.sim([test])
print((out[0] == target[0]).all(), 'Sim. steps', len(net.layers[0].outs))

print("\nTest on defaced O:")
test = np.asfarray([0, 1, 1, 1, 0,
                    1, 0, 0, 0, 1,
                    1, 0, 0, 0, 1,
                    1, 0, 0, 0, 1,
                    0, 1, 1, 1, 0])
test[test == 0] = -1
out = net.sim([test])
print((out[0] == target[0]).all(), 'Sim. steps', len(net.layers[0].outs))
